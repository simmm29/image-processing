#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <mpi.h>

// filtrele
const float smooth[4][4] = {{0.11111111, 0.11111111, 0.11111111},
	{0.11111111, 0.11111111, 0.11111111},
	{0.11111111, 0.11111111, 0.11111111}};
const float blur[4][4] = {{0.0625, 0.125, 0.0625},
	{0.125, 0.25, 0.125},
	{0.0625, 0.125, 0.0625}};
const float sharpen[4][4] = {{0, -0.66666667, 0},
	{-0.66666667, 3.66666667, -0.66666667},
	{0, -0.66666667, 0}};
const float mean[4][4] = {{-1, -1, -1},
	{-1, 9, -1},
	{-1, -1, -1}};
const float emboss[4][4] = {{0, 1, 0},
	{0, 0, 0},
	{0, -1, 0}};

// matricea in care se va afla filtrul la un moment dat
float H[4][4];

void citire(FILE *fin, unsigned char **I, int height, int width){
	// citesc din fisierul de input si bordez matricea input
	for(int i = 0; i <= height + 1; i++)
		for(int j = 0; j <= width + 1; j++)
			if(i > 0 && j > 0 && i < height + 1 && j < width + 1)
				fscanf(fin, "%c", &I[i][j]);
			else
				I[i][j] = 0;
}

void citire_color(FILE* fin, unsigned char **I_1, unsigned char **I_2, unsigned char **I_3, int height, int width){
	for(int i = 0; i <= height + 1; i++)
		for(int j = 0; j <= width + 1; j++)
			if(i > 0 && j > 0 && i < height + 1 && j < width + 1)
				// citesc in cate o matrice de input diferita fiecare canal de culoare
				fscanf(fin, "%c%c%c", &I_1[i][j], &I_2[i][j], &I_3[i][j]);
			else{
				I_1[i][j] = 0;
				I_2[i][j] = 0;
				I_3[i][j] = 0;
			}
}


// stabilesc ce filtru am primit din linia de comanda
void filtre(int t, int argc, char *argv[], float H[4][4]){
	if(strcmp("smooth", argv[t]) == 0){
		for(int i = 1; i <= 3; i++)
			for(int j = 1; j <=3; j++)
				H[i][j] = smooth[i - 1][j - 1];	
	}
	if(strcmp("blur", argv[t]) == 0){
		for(int i = 1; i <= 3; i++)
			for(int j = 1; j <=3; j++)
				H[i][j] = blur[i - 1][j - 1];	
	}
	if(strcmp("sharpen", argv[t]) == 0){
		for(int i = 1; i <= 3; i++)
			for(int j = 1; j <=3; j++)
				H[i][j] = sharpen[i - 1][j - 1];	
	}
	if(strcmp("mean", argv[t]) == 0){
		for(int i = 1; i <= 3; i++)
			for(int j = 1; j <=3; j++)
				H[i][j] = mean[i - 1][j - 1];	
	}
	if(strcmp("emboss", argv[t]) == 0){
		for(int i = 1; i <= 3; i++)
			for(int j = 1; j <=3; j++)
				H[i][j] = emboss[i - 1][j - 1];	
	}
}

void procesare(int argc, char *argv[], int prim, int ultim, unsigned char **I, unsigned char **O, int height, int width){
	// atata timp cat mai am filtre in linia de comanda
	for(int t = 3; t < argc; t++){	
		filtre(t, argc, argv, H); // aflu filtrul curent
		if(t > 3){ // daca am mai mult de un filtru, atunci matricea de input I va deveni matricea de output O
			for(int i = prim  + 1; i <= ultim; i++)
				for(int j = 1; j <= width; j++)
					I[i][j] = O[i][j];
		}
	
		int p, q;		
		float e = 0;
		
		for(int i = prim + 1; i <= ultim; i++){
			for(int j = 1; j <= width; j++)
			{	//q, p sunt indicii matricii filtru rotita
				p = 3;
				q = 3;
				e = 0; // variabila in care tin pixelul curent modificat
				for(int k = i - 1; k <= i + 1; k++){
					for(int l = j - 1; l <= j + 1; l++){
						e += H[q][p] * (float)I[k][l]; // modific pixelul curent in functie de		
						p--; // el si vecinii lui
									
					}		
					p = 3;
					q--;		
				}
				// clamparea
				if(e >=255)
					e = 255;
				if(e <= 0)
					e = 0;
				O[i][j] = (unsigned char) e; // pun pixelul modificat in matricea de output O
			}
		}				
	}
}

// aceeasi abordare, dar fara impartirea matricii de input
void procesare_secvential(int argc, char *argv[], unsigned char **I, unsigned char **O, int height, int width){
	for(int t = 3; t < argc; t++){	
		filtre(t, argc, argv, H);		
		if(t > 3){
			for(int i = 1; i <= height; i++)
				for(int j = 1; j <= width; j++)
					I[i][j] = O[i][j];
		}
		int p, q;
		float e = 0;
		for(int i = 1; i <= height; i++){
			for(int j = 1; j <= width; j++)
			{
				p = 3;
				q = 3;
				e = 0;
				for(int k = i - 1; k <= i + 1; k++){
					for(int l = j - 1; l <= j + 1; l++){
						e += H[q][p] * (float)I[k][l];
						p--;			
					}	
					p = 3;
					q--;			
				}
				if(e >=255)
					e = 255;
				if(e <= 0)
					e = 0;
				O[i][j] = (unsigned char) e;
			}
		}		
	}
		
}

int main(int argc, char *argv[]){
	
	int ok = 0;
	int rank;
	int nProcesses;
	MPI_Init(&argc, &argv);
	MPI_Comm_rank(MPI_COMM_WORLD, &rank);
	MPI_Comm_size(MPI_COMM_WORLD, &nProcesses);
	int q = 3, p = 3, l;
	FILE *fin = fopen(argv[1], "rb"); // fisier de input
	FILE *fout = fopen(argv[2], "wb"); // fisier de output
	
	int width, height, maxval;
	char mode[3], mode_1[3], vector[46];
	unsigned char **I, **O, **I_1, **I_2, **I_3, **O_1, **O_2, **O_3;

	fgets(mode, 100, fin);
	fgets(vector, 100, fin);
	char buffer[50];
	fgets(buffer, 50, fin);
	sscanf(buffer, "%d %d", &width, &height);
	fgets(buffer, 50, fin);
	sscanf(buffer, "%d", &maxval);
	strncpy(mode_1, mode, 2);
	fprintf(fout, "%s\n", mode_1);
	fprintf(fout, "%d ", width);
	fprintf(fout, "%d\n", height);
	fprintf(fout, "%d\n", maxval);
	int count = 0;
	
	// daca rulez pe mai mult de 1 proces
	if(nProcesses > 1){
		// daca am imagine alb-negru
		if(strncmp("P5", mode, 2) == 0){
			I = (unsigned char**)malloc(sizeof(unsigned char**) * (height + 2));
			for(int i = 0; i <= height + 1; i++)
				I[i] = (unsigned char *) malloc(sizeof(unsigned char*) * (width + 2));

			O = (unsigned char**)malloc(sizeof(unsigned char**) * (height + 2));
			for(int i = 0; i <= height + 1; i++)
				O[i] = (unsigned char *) malloc(sizeof(unsigned char*) * (2 + width));
				
			 if(rank == 0){
				citire(fin, I, height, width);	
				// imi impart matricea de input I in nProcesses-1
				for(int f = 1; f < nProcesses; f++){	
					// pentru fiecare proces in afara de 0 calculez limitele (prim, ultim)	
					int prim = count;
					int ultim = fmin(height, (f)* (int)height/ (nProcesses - 1));
					count = ultim;
					// trimit partea aferenta de matrice ce va fi prelucrata fiecarui proces 
					for(int i = prim; i <= ultim + 1; i++)
						for(int j = 0; j <= width + 1; j++)
							MPI_Send(&I[i][j], 1, MPI_UNSIGNED_CHAR, f , 0, MPI_COMM_WORLD);
						
					// primesc partea de imagine prelucrata inapoi
					for(int i = prim + 1; i <= ultim; i++)
						for(int j = 1; j <= width; j++)
							MPI_Recv(&O[i][j], 1, MPI_UNSIGNED_CHAR, f, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
	
				}
				// scriu toata matricea de output in fisierul de output
				for(int i = 1; i <= height; i++)
					for(int j = 1; j <= width; j++)	
						fprintf(fout, "%c", O[i][j]);	
						
			}else{
				// calculez limitele procesului curent
				int prim = (rank - 1)* (int) height / (nProcesses - 1);
				int ultim = fmin(height, (rank)* (int)height/ (nProcesses - 1));
				
				// primesc partea de matrice de prelucrat de la procesul 0 (bordata)
				for(int i = prim; i <= ultim + 1; i++){
					for(int j = 0; j <= width  + 1; j++){	
						MPI_Recv(&I[i][j], 1, MPI_UNSIGNED_CHAR, 0, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
					}
				}
				// prelucrarea efectiva a partii primite
				procesare(argc, argv, prim, ultim, I, O, height, width);
				//trimit inapoi la procesul 0 partea de matrice prelucrata
				for(int i = prim + 1; i <= ultim; i++)
					for(int j = 1; j <= width; j++)
						MPI_Send(&O[i][j], 1, MPI_UNSIGNED_CHAR, 0, 0, MPI_COMM_WORLD);
			}
		}else{ // daca poza e color
			// aceeasi abordare, pe cele 3 canale de culori
			I_1 = (unsigned char**)malloc(sizeof(unsigned char**) * (2 + height));
				for(int i = 0; i <= height + 1; i++)
					I_1[i] = (unsigned char *) malloc(sizeof(unsigned char*) * (width + 2));
			I_2 = (unsigned char**)malloc(sizeof(unsigned char**) * (height + 2));
				for(int i = 0; i <= height + 1; i++)
					I_2[i] = (unsigned char *) malloc(sizeof(unsigned char*) * (width + 2));
			I_3 = (unsigned char**)malloc(sizeof(unsigned char**) * (height + 2));
				for(int i = 0; i <= height + 1; i++)
					I_3[i] = (unsigned char *) malloc(sizeof(unsigned char*) * (width + 2));

			O_1 = (unsigned char**)malloc(sizeof(unsigned char**) * ( height + 2));
			for(int i = 0; i <= 1 + height; i++)
				O_1[i] = (unsigned char *) malloc(sizeof(unsigned char*) * (2 + width));
			O_2 = (unsigned char**)malloc(sizeof(unsigned char**) * ( height + 2));
			for(int i = 0; i <= 1 + height; i++)
				O_2[i] = (unsigned char *) malloc(sizeof(unsigned char*) * (2 + width));
			O_3 = (unsigned char**)malloc(sizeof(unsigned char**) * ( height + 2));
			for(int i = 0; i <= 1 + height; i++)
				O_3[i] = (unsigned char *) malloc(sizeof(unsigned char*) * (2 + width));
			if(rank == 0){
				citire_color(fin, I_1, I_2, I_3, height, width);
				for(int f = 1; f < nProcesses; f++){		
					int prim = count;
					int ultim = fmin(height, f* (int)height/ (nProcesses - 1));
					count = ultim;
						
					for(int i = prim; i <= 1 + ultim; i++)
						for(int j = 0; j <= 1 + width; j++){
								MPI_Send(&I_1[i][j], 1, MPI_UNSIGNED_CHAR, f, 0, MPI_COMM_WORLD);
								MPI_Send(&I_2[i][j], 1, MPI_UNSIGNED_CHAR, f, 0, MPI_COMM_WORLD);
								MPI_Send(&I_3[i][j], 1, MPI_UNSIGNED_CHAR, f, 0, MPI_COMM_WORLD);
							}
					for(int i = prim + 1; i <= ultim; i++){
						for(int j = 1; j <= width; j++){
							MPI_Recv(&O_1[i][j], 1, MPI_UNSIGNED_CHAR, f, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
							MPI_Recv(&O_2[i][j], 1, MPI_UNSIGNED_CHAR, f, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
							MPI_Recv(&O_3[i][j], 1, MPI_UNSIGNED_CHAR, f, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
							
						}
					}
					
				}
				for(int i = 1; i <= height; i++)
						for(int j = 1; j <= width; j++)
							fprintf(fout, "%c%c%c", O_1[i][j], O_2[i][j], O_3[i][j]);
				
			}else{
				int prim = (rank - 1)* (int) height / (nProcesses - 1);
				int ultim = fmin(height, (rank)* (int)height/ (nProcesses - 1));
				for(int i = prim; i <= 1 + ultim; i++){
					for(int j = 0; j <= 1 + width; j++){	
						MPI_Recv(&I_1[i][j], 1, MPI_UNSIGNED_CHAR, 0, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
						MPI_Recv(&I_2[i][j], 1, MPI_UNSIGNED_CHAR, 0, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
						MPI_Recv(&I_3[i][j], 1, MPI_UNSIGNED_CHAR, 0, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
					}
				}
				procesare(argc, argv, prim, ultim, I_1, O_1, height, width);
				procesare(argc, argv, prim, ultim, I_2, O_2, height, width);
				procesare(argc, argv, prim, ultim, I_3, O_3, height, width);

				for(int i = prim + 1; i <= ultim; i++)
					for(int j = 1; j <= width; j++){
						MPI_Send(&O_1[i][j], 1, MPI_UNSIGNED_CHAR, 0, 0, MPI_COMM_WORLD);
						MPI_Send(&O_2[i][j], 1, MPI_UNSIGNED_CHAR, 0, 0, MPI_COMM_WORLD);
						MPI_Send(&O_3[i][j], 1, MPI_UNSIGNED_CHAR, 0, 0, MPI_COMM_WORLD);
					}	
		}
		for(int i = 0; i <= 1 + height; i++){
					free(I_1[i]);
					free(O_1[i]);
					free(I_2[i]);
					free(O_2[i]);
					free(I_3[i]);
					free(O_3[i]);
				}
				free(I_1);
				free(O_1);
				free(I_2);
				free(O_2);
				free(I_3);
				free(O_3);
		}		
	}else{	// daca am un singur proces
		if(strncmp("P5", mode, 2) == 0){
			I = (unsigned char**)malloc(sizeof(unsigned char**) * (2 + height));
			for(int i = 0; i <= 1 + height; i++)
				I[i] = (unsigned char *) malloc(sizeof(unsigned char*) * (2 + width));


			O = (unsigned char**)malloc(sizeof(unsigned char**) * (2 + height));
			for(int i = 0; i <= 1 + height; i++)
				O[i] = (unsigned char *) malloc(sizeof(unsigned char*) * (2 + width));

			citire(fin, I, height, width);
			// se face procesare intregii matrici odata
			procesare_secvential(argc, argv, I, O, height, width);

			// scrierea imaginii procesate in fisierul de output
			for(int i = 1; i <= height; i++)
				for(int j = 1; j <= width; j++)
					fprintf(fout, "%c", O[i][j]);

			// eliberarea memoriei		
			for(int i = 0; i <= height + 1; i++){
				free(I[i]);
				free(O[i]);
			}
			free(I);
			free(O);
		}else{
			
			I_1 = (unsigned char**)malloc(sizeof(unsigned char**) * (2 + height));
				for(int i = 0; i <= 1 + height; i++)
					I_1[i] = (unsigned char *) malloc(sizeof(unsigned char*) * (2 + width));
			I_2 = (unsigned char**)malloc(sizeof(unsigned char**) * (2 + height));
				for(int i = 0; i <= 1 + height; i++)
					I_2[i] = (unsigned char *) malloc(sizeof(unsigned char*) * (2 + width));
			I_3 = (unsigned char**)malloc(sizeof(unsigned char**) * (2 + height));
				for(int i = 0; i <= 1 + height; i++)
					I_3[i] = (unsigned char *) malloc(sizeof(unsigned char*) * (2 + width));

			O_1 = (unsigned char**)malloc(sizeof(unsigned char**) * (2 + height));
			for(int i = 0; i <= height + 1; i++)
				O_1[i] = (unsigned char *) malloc(sizeof(unsigned char*) * (2 + width));
			O_2 = (unsigned char**)malloc(sizeof(unsigned char**) * (2 + height));
			for(int i = 0; i <= height + 1; i++)
				O_2[i] = (unsigned char *) malloc(sizeof(unsigned char*) * (2 + width));
			O_3 = (unsigned char**)malloc(sizeof(unsigned char**) * (2 + height));
			for(int i = 0; i <= height + 1; i++)
				O_3[i] = (unsigned char *) malloc(sizeof(unsigned char*) * (2 + width));
			
			citire_color(fin, I_1, I_2, I_3, height, width);
			procesare_secvential(argc, argv, I_1, O_1, height, width);
			procesare_secvential(argc, argv, I_2, O_2, height, width);
			procesare_secvential(argc, argv, I_3, O_3, height, width);
	
			for(int i = 1; i <= height; i++){
				for(int j = 1; j <= width; j++){
					fprintf(fout, "%c%c%c", O_1[i][j], O_2[i][j], O_3[i][j]);
				}
			}

			// eliberarea memoriei
			for(int i = 0; i <= 1 + height; i++){
				free(I_1[i]);
				free(O_1[i]);
				free(I_2[i]);
				free(O_2[i]);
				free(I_3[i]);
				free(O_3[i]);
			}
			free(I_1);
			free(O_1);
			free(I_2);
			free(O_2);
			free(I_3);
			free(O_3);
			}	
	}
	fclose(fin);
	fclose(fout);

	MPI_Finalize();
	return 0;
}		
